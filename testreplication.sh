#!/bin/bash
MASTER_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress}}'  mysql-master)
SLAVE_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress}}'  mysql-slave)
echo "master: $MASTER_IP"
echo "slave: $SLAVE_IP"

MASTER_PORT=3306
SLAVE_PORT=3306
MASTER="mysql -u admin -psecret -h $MASTER_IP -P $MASTER_PORT"
SLAVE="mysql -u admin -psecret -h $SLAVE_IP -P $SLAVE_PORT"

echo "# Creating a table in the master"
$MASTER -e 'create schema if not exists test'
$MASTER -ve 'drop table if exists test.t1'
$MASTER -ve ' create table t1 (i int not null primary key, msg varchar(50), d date, t time, dt datetime);' test
$MASTER -ve " insert into t1 values (1, 'test1', current_date(), now() + interval 11 second, now());" test
sleep 1
echo "# Retrieving the table from the slave"
$SLAVE -e 'select * from test.t1'
